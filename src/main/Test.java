package main;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
    String[] arrString = new String[0];
    void workString() {
        String bigString = null;
        String smallString = null;
        if (arrString.length == 0 || arrString.length == 1) {
            System.out.println(Arrays.toString(arrString));
        } else {
            for (int i = 0; i != arrString.length; i++) {
                for (int j = 0; j != arrString.length - 1; j++) {
                    if (arrString[j + 1].length() < arrString[j].length()) {
                        String temp = arrString[j];
                        arrString[j] = arrString[j + 1];
                        arrString[j + 1] = temp;
                    }
                }
            }
            smallString = arrString[0];
            bigString = arrString[arrString.length-1];
        }
        if (bigString != null) {
            System.out.println("Строка " + "[" + bigString + "]" + " самая большая с длинной: " + bigString.length() + " элементов");
        }
        if (smallString != null) {
            System.out.println("Строка " + "[" + smallString + "]" + " самая маленькая с длинной: " + smallString.length() + " элементов");
        }
    }

    String[] fillArrayString() {
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        String exit = "e";
        String string = null;
        System.out.println("Для выхода нажмите 'e' ");
        while (true) {
            if (number == 0) {
                number++;
                System.out.println("Введите первую строку");
                string = scanner.nextLine();
                if (string.equals(exit)) {
                    return arrString;
                }
                addElememt( string);
            } else {
                System.out.println("Введите следующую строку");
                string = scanner.nextLine();
                if (string.equals(exit)) {
                    return arrString;
                }
                addElememt( string);
            }

        }
    }

   void addElememt(String element) {
        String[] temp = new String[arrString.length + 1];
        for (int i = 0; i != arrString.length; i++) {
            temp[i] = arrString[i];
        }
        for (int i = arrString.length; i <temp.length ; i++) {
            temp[i] = element;
        }
        arrString = temp;
    }
    void sort(){
        for (int i = 0; i < arrString.length ; i++) {
            for (int j = 0; j <arrString.length-1 ; j++) {
                if(arrString[j].length()>arrString[j+1].length()){
                    String temp = arrString[j];
                    arrString[j] = arrString[j+1];
                    arrString[j+1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arrString));
    }
    void regularSynt(){
        if(arrString.length==0){
            System.out.println("Пустой массив");
        }else{
            System.out.println("Введите цифру от 1 до 8: \n " +
                    "1)Нижний регистр \n" +
                    "2)Верхний регистр \n" +
                    "3)Символьные \n" +
                    "4)abc \n" +
                    "5)xyz \n" +
                    "6)Начало с 987 \n" +
                    "7)Символов меньше чем 5 \n" +
                    "8)Символов больше чем 5 ");
            Scanner scanner = new Scanner(System.in);
            int key = scanner.nextByte();
            Pattern p ;
            Matcher m;
            switch (key) {
                case 1:
                    // Нижнее
                    p = Pattern.compile("[a-z]+");
                    break;
                case 2:
                    //Верхнее
                    p = Pattern.compile("[A-Z]+");
                    break;
                case 3:
                    //Символьные
                    p = Pattern.compile("[a-zA-Z]+");
                    break;
                case 4:
                    //abc
                    p = Pattern.compile("[abc]+");
                    break;
                case 5:
                    //xyz
                    p = Pattern.compile("[xyz]+");
                    break;
                case 6:
                    //987
                    p = Pattern.compile("^[987]\\W+");
                    break;
                case 7:
                    //<=5
                    p = Pattern.compile("\\w{0,5}+");
                    break;
                case 8:
                    //>=5
                    p = Pattern.compile("\\w{5,100}+");
                    break;
                default:
                    System.out.println("Введите от 1 до 8");
                    return;
            }
            for (int i = 0; i < arrString.length; i++) {
                String check = arrString[i];
                m = p.matcher(check);
                System.out.println( check+"; key = " +key+"; answer = "+ m.matches());
            }
        }
    }
    void email() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите email: ");
        String email = scanner.nextLine();
        Pattern p = Pattern.compile("[a-z]*\\d*[a-z0-9]{10,100}+(@\\.com)$");
        Matcher m = p.matcher(email);
        System.out.println(m.matches());
    }
}
