package main;

public class WorkStrings {
    //1
    String concat(String first, String second) {
        return first + second;
    }
    //2
    String valueOf(Object obj) {
        return obj.toString();
    }

    //3
    String join(String first, String second, char dec) {
        char[] f = first.toCharArray();
        char[] s = second.toCharArray();
        String temp = concat(first, second);
        for (int i = 0; i != f.length; i++) {
            if (f[i] == dec) {
                f[i] = ' ';
            }
        }
        for (int i = 0; i != s.length; i++) {
            if (s[i] == dec) {
                s[i] = ' ';
            }
        }
        first = String.valueOf(f);
        second = String.valueOf(s);
        return concat(first, second);
    }

    //4
    Boolean compare(String first, String second) {
        return first.equals(second);
    }

    char charAt(String string, int index) {
        char temp = 0;
        char[] array = string.toCharArray();
        for (int i = 0; i != array.length; i++) {
            if (index == i) {
                temp = array[i];
            }
        }
        return temp;
    }
    //5
    char[] getChars(String string) {
        char[] charArr = string.toCharArray();
        return charArr;
    }
    //6
    boolean equals(String first, String second) {
        boolean is_equals = true;
        char[] f = getChars(first);
        char[] s = getChars(second);
        int size = 0;
        if (f.length == s.length) {
            size = f.length;
        } else {
            if (f.length > s.length) {
                size = f.length;
            } else {
                size = s.length;
            }
        }
        for (int i = 0; i < size; i++) {
            if ((int) f[i] != (int) s[i]) {
                is_equals = false;
            }
        }
        return is_equals;
    }
    //7
    boolean equalsIgnoreCase(String first, String second) {
        boolean is_equals = true;
        char[] f = getChars(first);
        char[] s = getChars(second);
        int size = 0;
        if (f.length == s.length) {
            size = f.length;
        } else {
            if (f.length > s.length) {
                size = f.length;
            } else {
                size = s.length;
            }
            for (int i = 0; i < size; i++) {
                if (f[i] != s[i]) {
                    is_equals = false;
                }
            }
        }
        return is_equals;
    }
}
